# Generated by Django 3.2.3 on 2021-07-30 03:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20210728_2122'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producto',
            name='stock_critico',
        ),
        migrations.RemoveField(
            model_name='producto',
            name='vencimiento',
        ),
    ]
